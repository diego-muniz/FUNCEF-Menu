# Componente - FUNCEF-Menu

## Conteúdos

1. [Descrição](#descrição)
2. [Instalação](#instalação)
3. [Script](#script)
4. [CSS](#css)
5. [Módulo](#módulo)
6. [Uso](#uso)
7. [Parâmetros](#parâmetros)
8. [Desinstalação](#desinstalação)


## Descrição

- Componente para apresentação de Menu lateral com opção para submenus.

 
## Instalação:

### Bower

- Necessário executar o bower install e passar o nome do componente.

```
bower install funcef-menu --save.
```

## Script

```html
<script src="bower_components/funcef-menu/dist/funcef-menu.js"></script>
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## CSS  

```html
<link rel="stylesheet" href="bower_components/funcef-menu/dist/funcef-menu.css">
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## Módulo

- Adicione funcef-menu dentro do módulo do Sistema.

```js
    angular
        .module('funcef-demo', ['funcef-menu']);
```

## Uso

```html
<ngf-menu options="vm.menuOptions" icons="vm.icons"></ngf-menu>
```

### Parâmetros:

- options (Obrigatório);
- icons (Opcional);

## Desinstalação:

```
bower uninstall funcef-menu --save
```