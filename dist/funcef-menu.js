(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name Validate
    * @version 1.0.0
    * @Componente para validação de formulários
    */
    angular.module('funcef-menu.controller', []);
    angular.module('funcef-menu.directive', []);

    angular
    .module('funcef-menu', [
      'funcef-menu.controller',
      'funcef-menu.directive',
      'ngCookies'
    ]);
})();;(function () {
    'use strict';

    angular.module('funcef-menu.controller').
        controller('MenuController', MenuController);

    MenuController.$inject = ['$rootScope', '$cookies', '$scope'];

    /* @ngInject */
    function MenuController($rootScope, $cookies, $scope) {

        var vm = this;
        init();
        //////////

        function init() {
            $rootScope.menuOpen = $cookies.get('menuOpen') == 'true';
        }
    }
})();;(function () {
    'use strict';

    angular
      .module('funcef-menu.directive')
      .directive('ngfMenu', ngfMenu);

    /* @ngInject */
    function ngfMenu() {
        return {
            restrict: 'EA',
            replace: true,
            transclude: true,
            templateUrl: 'views/menu.view.html',
            controller: 'MenuController',
            controllerAs: 'vm',
            scope: {
                options: '=',
                icons: '='
            },
            link: function (scope, el, attrs, ctrl, transclude) {
                el.find('.content').append(transclude());
            }
        };
    }
})();;angular.module('funcef-menu').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/menu.view.html',
    "<aside id=\"aside\" ng-show=\"options.length\"> <nav class=\"navbar navbar-black sidebar\" ng-class=\"{ 'open': $root.menuOpen }\" role=\"navigation\"> <div class=\"container-fluid\"> <div class=\"collapse navbar-collapse\" id=\"bs-sidebar-navbar-collapse-1\"> <ul class=\"nav navbar-nav\"> <li ng-repeat=\"item in options track by $index\" class=\"dropdown\"> <a ui-sref=\"{{item.Itens.length ? '' : item.Descricao}}\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"> <i class=\"hidden-xs showopacity {{icons[item.Id]}}\"></i> <span class=\"item-text\">{{item.Nome}}</span> <span ng-if=\"item.Itens.length\" class=\"caret\"></span> </a> <ul role=\"menu\" ng-if=\"item.Itens.length\"> <li ng-repeat=\"itemMenu in item.Itens track by $index\"> <a ui-sref=\"{{itemMenu.Descricao}}\"> {{itemMenu.Nome}} </a> </li> </ul> </li> </ul> </div> </div> </nav> </aside>"
  );

}]);
