﻿(function () {
    'use strict';

    angular.module('funcef-menu.controller').
        controller('MenuController', MenuController);

    MenuController.$inject = ['$rootScope', '$cookies', '$scope'];

    /* @ngInject */
    function MenuController($rootScope, $cookies, $scope) {

        var vm = this;
        init();
        //////////

        function init() {
            $rootScope.menuOpen = $cookies.get('menuOpen') == 'true';
        }
    }
})();