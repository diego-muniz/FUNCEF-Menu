﻿(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name Menu
    * @version 1.0.0
    * @Componente Menu de formulários
    */
    angular.module('funcef-menu.controller', []);
    angular.module('funcef-menu.directive', []);

    angular
    .module('funcef-menu', [
      'funcef-menu.controller',
      'funcef-menu.directive',
      'ngCookies'
    ]);
})();