﻿(function () {
    'use strict';

    angular
      .module('funcef-menu.directive')
      .directive('ngfMenu', ngfMenu);

    /* @ngInject */
    function ngfMenu() {
        return {
            restrict: 'EA',
            replace: true,
            transclude: true,
            templateUrl: 'views/menu.view.html',
            controller: 'MenuController',
            controllerAs: 'vm',
            scope: {
                options: '=',
                icons: '='
            },
            link: function (scope, el, attrs, ctrl, transclude) {
                el.find('.content').append(transclude());
            }
        };
    }
})();