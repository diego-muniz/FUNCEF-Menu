﻿(function () {
    'use strict';

    angular.module('funcef-demo.controller')
        .controller('Example1Controller', Example1Controller);

    Example1Controller.$inject = [];

    /* @ngInject */
    function Example1Controller() {
        var vm = this;

        vm.icons = {
	    	"1" : "fa fa-check",
	    	"2" : "fa fa-cog",
	    	"3" : "fa fa-edit"
	    };

        vm.menuOptions = [
		{
			"Id": 1,
			"Nome": "Opção 1",
			"Itens": [
				{
					"Id": 11,
					"Nome": "Opção 1 - 1"
				},
				{
					"Id": 12,
					"Nome": "Opção 1 - 2"
				}
			]
		},
		{
			"Id": 2,
			"Nome": "Opção 2",
			"Itens": [
				{
					"Id": 21,
					"Nome": "Opção 2 - 1"
				},
				{
					"Id": 22,
					"Nome": "Opção 2 - 2"
				}
			]
		},
		{
			"Id": 3,
			"Nome": "Opção 3",
			"Itens": [
				{
					"Id": 31,
					"Nome": "Opção 3 - 1"
				},
				{
					"Id": 32,
					"Nome": "Opção 3 - 2"
				}
			]
		}];
	}
}());